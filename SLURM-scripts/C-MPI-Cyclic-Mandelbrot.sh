#!/bin/bash
#SBATCH --partition=bigmem
#SBATCH --time=01:30:00
#SBATCH --mem=10G
#SBATCH --ntasks=20
#SBATCH --chdir=.
#SBATCH --output=SLURM-logs/C-MPI-Cyclic-Mandelbrot_%j.txt
#SBATCH --error=SLURM-logs/C-MPI-Cyclic-Mandelbrot_%j.txt
#SBATCH --job-name="C-MPI-Cyclic-Mandelbrot"

module load openmpi

echo -e "\e[34mmpirun --hostfile MPI/hostfile -n 20 C/bin/mandelbrot_mpi_cyclic.exe 0.001 5000\e[0m"
mpirun --hostfile MPI/hostfile -n 20 C/bin/mandelbrot_mpi_cyclic.exe 0.001 5000
