#!/bin/bash
#SBATCH --partition=bigmem
#SBATCH --time=01:30:00
#SBATCH --mem=10G
#SBATCH --cpus-per-task=20
#SBATCH --chdir=.
#SBATCH --output=SLURM-logs/C-pthread-Cyclic-Mandelbrot_%j.txt
#SBATCH --error=SLURM-logs/C-pthread-Cyclic-Mandelbrot_%j.txt
#SBATCH --job-name="C-pthread-Cyclic-Mandelbrot"

echo -e "\e[34m./C/bin/mandelbrot_pthread_cyclic.exe 0.001 5000 20\e[0m"
./C/bin/mandelbrot_pthread_cyclic.exe 0.001 5000 20
