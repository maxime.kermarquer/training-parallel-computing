#!/bin/bash
#SBATCH --partition=bigmem
#SBATCH --time=01:30:00
#SBATCH --mem=10G
#SBATCH --cpus-per-task=20
#SBATCH --chdir=.
#SBATCH --output=SLURM-logs/C-OpenMP-Mandelbrot_%j.txt
#SBATCH --error=SLURM-logs/C-OpenMP-Mandelbrot_%j.txt
#SBATCH --job-name="C-OpenMP-Mandelbrot"


echo -e "\e[34mexport OMP_NUM_THREADS=20\e[0m"
echo -e "\e[34m./C/bin/mandelbrot_omp.exe 0.001 5000\e[0m"

export OMP_NUM_THREADS=20
./C/bin/mandelbrot_omp.exe 0.001 5000
