#!/bin/bash
#SBATCH --partition=bigmem
#SBATCH --time=01:30:00
#SBATCH --mem=10G
#SBATCH --cpus-per-task=20
#SBATCH --chdir=.
#SBATCH --output=SLURM-logs/MATLAB-Parfor-Mandelbrot_%j.txt
#SBATCH --error=SLURM-logs/MATLAB-Parfor-Mandelbrot_%j.txt
#SBATCH --job-name="MATLAB-Parfor-Mandelbrot"

module load MATLAB

echo -e '\e[34mmatlab -nodesktop -softwareopengl -nosplash -nodisplay -r "addpath('MATLAB/src');  mandelbrot_parfor 0.001 5000 20"\e[0m'
matlab -nodesktop -softwareopengl -nosplash -nodisplay -r "addpath('MATLAB/src');  mandelbrot_parfor 0.001 5000 20"
