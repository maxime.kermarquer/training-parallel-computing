#!/bin/bash
#SBATCH --partition=bigmem
#SBATCH --time=01:30:00
#SBATCH --mem=10G
#SBATCH --cpus-per-task=1
#SBATCH --chdir=.
#SBATCH --output=SLURM-logs/Python-Serial-Mandelbrot_%j.txt
#SBATCH --error=SLURM-logs/Python-Serial-Mandelbrot_%j.txt
#SBATCH --job-name="Python-Serial-Mandelbrot"

module load python/3.6
echo -e "\e[34mpython Python/src/mandelbrot.py\e[0m"
python Python/src/mandelbrot.py
