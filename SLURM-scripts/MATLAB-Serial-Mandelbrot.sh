#!/bin/bash
#SBATCH --partition=bigmem
#SBATCH --time=01:30:00
#SBATCH --mem=10G
#SBATCH --cpus-per-task=1
#SBATCH --chdir=.
#SBATCH --output=SLURM-logs/MATLAB-Serial-Mandelbrot_%j.txt
#SBATCH --error=SLURM-logs/MATLAB-Serial-Mandelbrot_%j.txt
#SBATCH --job-name="MATLAB-Serial-Mandelbrot"

module load MATLAB

echo -e '\e[34mmatlab -nodesktop -softwareopengl -nosplash -nodisplay -r "addpath('MATLAB/src');  mandelbrot 0.001 5000"\e[0m'
matlab -nodesktop -softwareopengl -nosplash -nodisplay -r "addpath('MATLAB/src');  mandelbrot 0.001 5000"
