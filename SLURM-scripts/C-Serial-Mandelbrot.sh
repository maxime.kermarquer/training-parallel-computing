#!/bin/bash
#SBATCH --partition=bigmem
#SBATCH --time=01:30:00
#SBATCH --mem=10G
#SBATCH --cpus-per-task=1
#SBATCH --chdir=.
#SBATCH --output=SLURM-logs/C-Serial-Mandelbrot_%j.txt
#SBATCH --error=SLURM-logs/C-Serial-Mandelbrot_%j.txt
#SBATCH --job-name="C-Serial-Mandelbrot"

echo -e "\e[34m./C/bin/mandelbrot.exe 0.001 5000\e[0m"
./C/bin/mandelbrot.exe 0.001 5000
