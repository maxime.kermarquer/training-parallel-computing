

import sys
import math
import time
import numpy
from mpi4py import MPI

def main():
	############################
	### VARIABLE DEFINITIONS ###
	############################
	XMIN=-2
	YMIN=-2
	XMAX=2 
	YMAX=2 
	RESOLUTION=0.001
	NITERMAX=5000
	xpixel=0
	ypixel=0 
	OUTFILE = "results/Python-MPI-Cyclic-Mandelbrot.out"

	comm=MPI.COMM_WORLD
	rank=comm.Get_rank()
	size=comm.Get_size()
	


	if len(sys.argv) > 2:
		RESOLUTION = float(sys.argv[1])
		NITERMAX = int(sys.argv[2])

	nbpixelx = math.ceil((XMAX - XMIN) / RESOLUTION)
	nbpixely = math.ceil((YMAX - YMIN) / RESOLUTION)

	if rank == 0:
		print("---- Start of the Python MPI Cyclic program ----\nRESOLUTION = %f, NBITERMAX = %d, nbpixel = %d, NB_Process_MPI = %d.\n" % (RESOLUTION, NITERMAX, nbpixelx*nbpixely, size))


	if rank == 0:
		print("----  Start of the points computation ----")
		start = time.time()

	subitertab = numpy.zeros((int(nbpixelx/size), nbpixely))

	####################
	### COMPUTE PART ###
	####################
	pixel_columns_processed = 0
	for xpixel in range (rank, nbpixelx, size) :
		for ypixel in range (0, nbpixely) :
			xinit = XMIN + xpixel * RESOLUTION
			yinit = YMIN + ypixel * RESOLUTION

			x=xinit
			y=yinit
			iter=0

			for iter in range (0, NITERMAX+1) :
				prevy=y
				prevx=x
				if( (x*x + y*y) > 4 ) :
					break

				x = prevx*prevx - prevy*prevy + xinit

				y = 2*prevx*prevy + yinit 
			
			subitertab[pixel_columns_processed , ypixel] = iter
		pixel_columns_processed += 1
		

	itertab = comm.gather(subitertab,root=0)

	if rank == 0:
		end = time.time()
		print("The computation lasted ",end-start," seconds.\n---- End of the points computation ----\n")


	#############################
	### WRITE THE FILE OUTPUT ###
	#############################

	if rank == 0:

		itertab = numpy.concatenate(itertab)

		# Re-order the itertab according the proper xpixel indexes
		itertab_reordered = numpy.zeros((nbpixelx, nbpixely))
		xpixel = 0
		nb_col_by_proc = int(nbpixelx / size)
		for col_id in range (0, nb_col_by_proc) :
			for proc_id in range(0, size) :
				# Copy all columns of #proc id at the proper indexes
				for ypixel in range (0, nbpixely) :
					itertab_reordered[xpixel , ypixel] = itertab [ (proc_id*nb_col_by_proc+col_id) , ypixel];
				xpixel += 1
			

		print("---- Writing the file %s -----" % OUTFILE)

		file = open(OUTFILE, 'w')

		for xpixel in range (0,nbpixelx) :
			for ypixel in range (0, nbpixely) :
				x = XMIN + xpixel * RESOLUTION
				y = YMIN + ypixel * RESOLUTION
				file.write("%f %f %d\n"  % (x, y, itertab_reordered[xpixel,ypixel]) )
			file.write("\n")

		file.close()

		print("---- End of the program ----")

	sys.exit(0)

if __name__ == '__main__':
	main()