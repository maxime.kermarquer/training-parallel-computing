

import sys
import math
import time
import numpy


def main():
	############################
	### VARIABLE DEFINITIONS ###
	############################
	XMIN=-2
	YMIN=-2
	XMAX=2 
	YMAX=2 
	RESOLUTION=0.001
	NITERMAX=5000
	xpixel=0
	ypixel=0 
	OUTFILE = "results/Python-Serial-Mandelbrot.out"
	

	if len(sys.argv) > 2:
		RESOLUTION = float(sys.argv[1])
		NITERMAX = int(sys.argv[2])

	nbpixelx = math.ceil((XMAX - XMIN) / RESOLUTION)
	nbpixely = math.ceil((YMAX - YMIN) / RESOLUTION)

	print("---- Start of the Python Serial program ----\nRESOLUTION = ", RESOLUTION, ", NITERMAX = ", NITERMAX ,", nbpixel = ", nbpixelx*nbpixely, ".\n\n", sep="", flush=True)

	
	print("----  Start of the points computation ----")

	start = time.time()

	itertab = numpy.zeros((nbpixelx, nbpixely))

	####################
	### COMPUTE PART ###
	####################
	for xpixel in range (0,nbpixelx) :
		for ypixel in range (0, nbpixely) :
			xinit = XMIN + xpixel * RESOLUTION
			yinit = YMIN + ypixel * RESOLUTION

			x=xinit
			y=yinit
			iter=0

			for iter in range (0, NITERMAX+1) :
				prevy=y
				prevx=x
				if( (x*x + y*y) > 4 ) :
					break

				x = prevx*prevx - prevy*prevy + xinit

				y = 2*prevx*prevy + yinit 
			
			itertab[xpixel,ypixel] = iter  
		

	end = time.time()
	print("The computation lasted ",end-start," seconds.\n---- End of the points computation ----\n\n")

	#############################
	### WRITE THE FILE OUTPUT ###
	#############################

	print("---- Writing the file %s -----\n" % OUTFILE)

	file = open(OUTFILE, 'w')

	for xpixel in range (0,nbpixelx) :
		for ypixel in range (0, nbpixely) :
			x = XMIN + xpixel * RESOLUTION
			y = YMIN + ypixel * RESOLUTION
			file.write("%f %f %d\n"  % (x, y, itertab[xpixel,ypixel]) )
		file.write("\n")

	file.close()

	print("---- End of the program ----")

	sys.exit(0)

if __name__ == '__main__':
	main()