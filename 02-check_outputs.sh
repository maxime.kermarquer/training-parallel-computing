
## C
echo -e "\e[34mdiff -q results/C-Serial-Mandelbrot.out results/C-pthread-Block-Mandelbrot.out\e[0m"
diff -q results/C-Serial-Mandelbrot.out results/C-pthread-Block-Mandelbrot.out
echo -e "\e[34mdiff -q results/C-Serial-Mandelbrot.out results/C-pthread-Cyclic-Mandelbrot.out\e[0m"
diff -q results/C-Serial-Mandelbrot.out results/C-pthread-Cyclic-Mandelbrot.out
echo -e "\e[34mdiff -q results/C-Serial-Mandelbrot.out results/C-OpenMP-Mandelbrot.out\e[0m"
diff -q results/C-Serial-Mandelbrot.out results/C-OpenMP-Mandelbrot.out
echo -e "\e[34mdiff -q results/C-Serial-Mandelbrot.out results/C-MPI-Block-Mandelbrot.out\e[0m"
diff -q results/C-Serial-Mandelbrot.out results/C-MPI-Block-Mandelbrot.out
echo -e "\e[34mdiff -q results/C-Serial-Mandelbrot.out results/C-MPI-Cyclic-Mandelbrot.out\e[0m"
diff -q results/C-Serial-Mandelbrot.out results/C-MPI-Cyclic-Mandelbrot.out

## MATLAB
echo -e "\e[34mdiff -q results/C-Serial-Mandelbrot.out results/MATLAB-Serial-Mandelbrot.out\e[0m"
diff -q results/C-Serial-Mandelbrot.out results/MATLAB-Serial-Mandelbrot.out
echo -e "\e[34mdiff -q results/C-Serial-Mandelbrot.out results/MATLAB-Parfor-Mandelbrot.out\e[0m"
diff -q results/C-Serial-Mandelbrot.out results/MATLAB-Parfor-Mandelbrot.out


## Python
echo -e "\e[34mdiff -q results/C-Serial-Mandelbrot.out results/Python-Serial-Mandelbrot.out\e[0m"
diff -q results/C-Serial-Mandelbrot.out results/Python-Serial-Mandelbrot.out
echo -e "\e[34mdiff -q results/C-Serial-Mandelbrot.out results/Python-MPI-Cyclic-Mandelbrot.out\e[0m"
diff -q results/C-Serial-Mandelbrot.out results/Python-MPI-Cyclic-Mandelbrot.out
echo -e "\e[34mdiff -q results/C-Serial-Mandelbrot.out results/Python-MPI-Block-Mandelbrot.out\e[0m"
diff -q results/C-Serial-Mandelbrot.out results/Python-MPI-Block-Mandelbrot.out
