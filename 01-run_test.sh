#!/bin/bash

export NB_CORES_AVAILABLE=4

if [ ! -z ${1} ]; then
	export NB_CORES_AVAILABLE=${1}
fi


# Launch different code versions
echo -e "\e[34m./C/bin/mandelbrot.exe 0.001 5000\e[0m"
./C/bin/mandelbrot.exe 0.001 5000
echo -e "\e[34mexport OMP_NUM_THREADS=${NB_CORES_AVAILABLE}\e[0m"
echo -e "\e[34m./C/bin/mandelbrot_omp.exe 0.001 5000\e[0m"
export OMP_NUM_THREADS=${NB_CORES_AVAILABLE}
./C/bin/mandelbrot_omp.exe 0.001 5000
echo -e "\e[34m./C/bin/mandelbrot_pthread_block.exe 0.001 5000 ${NB_CORES_AVAILABLE}\e[0m"
./C/bin/mandelbrot_pthread_block.exe 0.001 5000 ${NB_CORES_AVAILABLE}
echo -e "\e[34m./C/bin/mandelbrot_pthread_cyclic.exe 0.001 5000 ${NB_CORES_AVAILABLE}\e[0m"
./C/bin/mandelbrot_pthread_cyclic.exe 0.001 5000 ${NB_CORES_AVAILABLE}
echo -e "\e[34mmpirun --hostfile MPI/hostfile -n ${NB_CORES_AVAILABLE} C/bin/mandelbrot_mpi_block.exe 0.001 5000\e[0m"
mpirun --hostfile MPI/hostfile -n ${NB_CORES_AVAILABLE} C/bin/mandelbrot_mpi_block.exe 0.001 5000
echo -e "\e[34mmpirun --hostfile MPI/hostfile -n ${NB_CORES_AVAILABLE} C/bin/mandelbrot_mpi_cyclic.exe 0.001 5000\e[0m"
mpirun --hostfile MPI/hostfile -n ${NB_CORES_AVAILABLE} C/bin/mandelbrot_mpi_cyclic.exe 0.001 5000


## Validate the results from the C-Serial reference
echo -e "\e[34mdiff -q results/C-Serial-Mandelbrot.out results/C-pthread-Block-Mandelbrot.out\e[0m"
diff -q results/C-Serial-Mandelbrot.out results/C-pthread-Block-Mandelbrot.out
echo -e "\e[34mdiff -q results/C-Serial-Mandelbrot.out results/C-pthread-Cyclic-Mandelbrot.out\e[0m"
diff -q results/C-Serial-Mandelbrot.out results/C-pthread-Cyclic-Mandelbrot.out
echo -e "\e[34mdiff -q results/C-Serial-Mandelbrot.out results/C-OpenMP-Mandelbrot.out\e[0m"
diff -q results/C-Serial-Mandelbrot.out results/C-OpenMP-Mandelbrot.out
echo -e "\e[34mdiff -q results/C-Serial-Mandelbrot.out results/C-MPI-Block-Mandelbrot.out\e[0m"
diff -q results/C-Serial-Mandelbrot.out results/C-MPI-Block-Mandelbrot.out
echo -e "\e[34mdiff -q results/C-Serial-Mandelbrot.out results/C-MPI-Cyclic-Mandelbrot.out\e[0m"
diff -q results/C-Serial-Mandelbrot.out results/C-MPI-Cyclic-Mandelbrot.out


# MATLAB
echo -e '\e[34mmatlab -nodesktop -softwareopengl -nosplash -nodisplay -r "addpath('MATLAB/src');  mandelbrot 0.001 5000"\e[0m'
matlab -nodesktop -softwareopengl -nosplash -nodisplay -r "addpath('MATLAB/src');  mandelbrot 0.001 5000"
echo -e '\e[34mmatlab -nodesktop -softwareopengl -nosplash -nodisplay -r "addpath('MATLAB/src');  mandelbrot_parfor 0.001 5000 20"\e[0m'
matlab -nodesktop -softwareopengl -nosplash -nodisplay -r "addpath('MATLAB/src');  mandelbrot_parfor 0.001 5000 20"


echo -e "diff -q results/C-Serial-Mandelbrot.out results/MATLAB-Serial-Mandelbrot.out"
diff -q results/C-Serial-Mandelbrot.out results/MATLAB-Serial-Mandelbrot.out
echo -e "diff -q results/C-Serial-Mandelbrot.out results/MATLAB-Parfor-Mandelbrot.out"
diff -q results/C-Serial-Mandelbrot.out results/MATLAB-Parfor-Mandelbrot.out


## Python

echo -e "\e[34mpython Python/src/mandelbrot.py\e[0m"
python Python/src/mandelbrot.py
echo -e "\e[34mmpirun --hostfile MPI/hostfile -n ${NB_CORES_AVAILABLE} python Python/src/mandelbrot_mpi_block.py 0.001 5000\e[0m"
mpirun --hostfile MPI/hostfile -n ${NB_CORES_AVAILABLE} python Python/src/mandelbrot_mpi_block.py 0.001 5000
echo -e "\e[34mmpirun --hostfile MPI/hostfile -n ${NB_CORES_AVAILABLE} python Python/src/mandelbrot_mpi_cyclic.py 0.001 5000\e[0m"
mpirun --hostfile MPI/hostfile -n ${NB_CORES_AVAILABLE} python Python/src/mandelbrot_mpi_cyclic.py 0.001 5000


## Validate Python results from the C-Serial reference
echo -e "\e[34mdiff -q results/C-Serial-Mandelbrot.out results/Python-Serial-Mandelbrot.out\e[0m"
diff -q results/C-Serial-Mandelbrot.out results/Python-Serial-Mandelbrot.out
echo -e "\e[34mdiff -q results/C-Serial-Mandelbrot.out results/Python-MPI-Cyclic-Mandelbrot.out\e[0m"
diff -q results/C-Serial-Mandelbrot.out results/Python-MPI-Cyclic-Mandelbrot.out
echo -e "\e[34mdiff -q results/C-Serial-Mandelbrot.out results/Python-MPI-Block-Mandelbrot.out\e[0m"
diff -q results/C-Serial-Mandelbrot.out results/Python-MPI-Block-Mandelbrot.out
