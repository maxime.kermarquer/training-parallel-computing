
## Training Parallel Computing

This repository aims to show the different ways to parallize code, through the Mandelbrot example [(wikipedia)](https://fr.wikipedia.org/wiki/Ensemble_de_Mandelbrot).


![](results/mandelbrot.png)

#### To run locally

* You need :
	* gcc
	* OpenMPI 
	* Python 3.7 with `mpi4py` and `numpy` packages.

* Run `01-run_test.sh` script

#### To run examples on ICM Cluster

* Load git and compilers.
~~~bash
module load git gcc openmpi
~~~
* Clone the repository.
~~~bash
git clone https://gitlab.com/maxime.kermarquer/training-parallel-computing
~~~
* Build C binaries.
~~~bash
mkdir C/bin
make -C C
~~~
* Submit jobs.
~~~bash
./03-submit.sh
~~~
* After all jobs completed, you can check the results are correct.
~~~bash
./02-check_outputs.sh
~~~
* Compare the compute times.


