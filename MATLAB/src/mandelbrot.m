function mandelbrot(RESOLUTION, NITERMAX) 


    %% Variables definitions

    OUTFILE = "results/MATLAB-Serial-Mandelbrot.out";

    XMIN = -2;
    YMIN = -2;
    XMAX = 2; 
    YMAX = 2; 
    
    
    % Cast arguments
    if nargin ~= 2
        RESOLUTION = 0.001;
        NITERMAX = 5000;
    else
        RESOLUTION = str2double(RESOLUTION);
        NITERMAX = str2num(NITERMAX); %#ok<ST2NM>
    end
    

    % Computation of the number of pixels
    nbpixelx = ceil((XMAX - XMIN) / RESOLUTION);
    nbpixely = ceil((YMAX - YMIN) / RESOLUTION);

    fprintf("---- Start of the MATLAB sequential program ----\nRESOLUTION = %f, NBITERMAX = %d, nbpixel = %d.\n\n", RESOLUTION, NITERMAX, nbpixelx*nbpixely);

    % Allocation of the pixel array
    itertab = zeros(nbpixelx, nbpixely);


    %% Points computation
    fprintf("----  Start of the points computation ----\n");
    tic;

    for xpixel = 1:nbpixelx
        for ypixel = 1:nbpixely
            xinit = XMIN + (xpixel-1) * RESOLUTION;
            yinit = YMIN + (ypixel-1) * RESOLUTION;

            x = xinit;
            y = yinit;
            iter=0;

            for iter = 0:NITERMAX
                prevy=y;
                prevx=x; 

                if( (x*x + y*y) > 4 )
                    break;
                end

                x= prevx*prevx - prevy*prevy + xinit;

                y= 2*prevx*prevy + yinit;
            end
            itertab(xpixel,ypixel) = iter; 
        end
    end

    duration = toc;

    fprintf("The computation lasted %f seconds.\n---- End of the points computation ----\n\n",duration);

    %% Output of the results in a file compatible with gnuplot
    fprintf("---- Writing the file %s -----\n", OUTFILE);
    
    file = fopen(OUTFILE,"w");

    for xpixel=1:nbpixelx
        for ypixel=1:nbpixely
            x = XMIN + (xpixel-1) * RESOLUTION; 
            y = YMIN + (ypixel-1) * RESOLUTION; 
            fprintf(file, "%f %f %d\n", x, y, itertab(xpixel,ypixel));
        end
        fprintf(file,"\n");
    end
    
    fclose(file);

    fprintf("---- End of the program ----\n");

    %% Program exit
    quit(0);
end