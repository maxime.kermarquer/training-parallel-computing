#!/bin/bash

export OPTIONS=--reservation=Parallel_Computing_Training

if [ -d SLURM-scripts ]; then
    for slurm_script in $(ls  SLURM-scripts); do
        echo -e "\e[34msbatch ${OPTIONS} SLURM-scripts/${slurm_script}\e[0m"
        sbatch --reservation=Parallel_Computing_Training  SLURM-scripts/${slurm_script}
    done
fi
