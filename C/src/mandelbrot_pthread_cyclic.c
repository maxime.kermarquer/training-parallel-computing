
// Libraries
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <errno.h> 
#include <sys/time.h>

#include <pthread.h>

// Define - Macros
#define OUTFILE "results/C-pthread-Cyclic-Mandelbrot.out"

// Thread parameters
struct thread_param {
	int cycle;
	int nbypixel;
	int nbxpixel;
	int * itertab;
	int NBTHREAD;
};


double XMIN=-2;
double YMIN=-2;
double XMAX=2; 
double YMAX=2; 
double RESOLUTION=0.001; 
int NITERMAX=5000;



// Thread function
void * compute (void *  p_arg){
	struct thread_param * arg = (struct thread_param *) p_arg;
	int intern_cycle = arg-> cycle;
	int intern_nbypixel = arg -> nbypixel;
	int intern_nbxpixel = arg -> nbxpixel;
	int * intern_itertab = arg -> itertab;
	int NBTHREAD = arg -> NBTHREAD;


	int xpixel, ypixel;
	for(xpixel=intern_cycle; xpixel < intern_nbxpixel; xpixel += NBTHREAD){

			double xinit = XMIN + xpixel * RESOLUTION;

			for(ypixel=0; ypixel<intern_nbypixel; ypixel++) {
				
				double yinit = YMIN + ypixel * RESOLUTION;
				double x=xinit; double y=yinit; int iter=0;

				for(iter=0; iter<NITERMAX; iter++) {
					double prevy=y, prevx=x; 

					if( (x*x + y*y) > 4 )
						break;

					x= prevx*prevx - prevy*prevy + xinit;
					y= 2*prevx*prevy + yinit;

				}
				intern_itertab[xpixel*intern_nbypixel+ypixel]=iter; 
			}

	}

}

int main(int argc, char **argv){

	int * itertab; 
	int nbpixelx; 
	int nbpixely;
	int xpixel=0, ypixel=0; 
	FILE * file;
	int NBTHREAD;

	if(argc > 3){
		RESOLUTION = atof(argv[1]);
		NITERMAX = atof(argv[2]);
		NBTHREAD = atoi(argv[3]);
	}
	else{
		NBTHREAD = 1;
	}

	// Computation of the number of pixels
	nbpixelx = ceil((XMAX - XMIN) / RESOLUTION); 
	nbpixely = ceil((YMAX - YMIN) / RESOLUTION);

	printf("---- Start of the pthread block program ----\nRESOLUTION = %f, NBITERMAX = %d, nbpixel = %d, NBTHREAD = %d.\n\n", RESOLUTION, NITERMAX, nbpixelx*nbpixely, NBTHREAD);


	// Allocation of the pixel array
	if( (itertab=malloc(sizeof(int)*nbpixelx*nbpixely)) == NULL){
		printf("itertab allocation ERROR, errno : %d (%s) .\n", errno, strerror(errno)); 
		return EXIT_FAILURE; 
	}


	// Points computation
	printf("----  Start of the points computation ----\n");
	struct timeval start, end, duration;
	gettimeofday(&start, NULL); 

	// Initialization and launch of threads
	int thread_id;
	struct thread_param threads_param[NBTHREAD];
	pthread_t threads[NBTHREAD]; 


	for(thread_id=0; thread_id<NBTHREAD; thread_id++){

		// Thread parameters
		threads_param[thread_id].cycle = thread_id;
		threads_param[thread_id].nbypixel = nbpixely;
		threads_param[thread_id].nbxpixel = nbpixelx;
		threads_param[thread_id].itertab = itertab;
		threads_param[thread_id].NBTHREAD = NBTHREAD;

		// Thread launch
		pthread_create(&threads[thread_id], NULL, compute, (void *) &threads_param[thread_id]);
	}

	// Thread grouping
	for(thread_id=0; thread_id<NBTHREAD;thread_id++){
    	void * ret_ptr;
    	pthread_join(threads[thread_id],&ret_ptr);
	}

	gettimeofday(&end,NULL);
	timersub(&end, &start, &duration);
	printf("The computation lasted %ld.%ld seconds.\n---- End of the points computation ----\n\n",(long int)duration.tv_sec,(long int)duration.tv_usec);

	// Output of the results in a file compatible with gnuplot
	printf("---- Writing the file %s -----\n", OUTFILE);
	if( (file=fopen(OUTFILE,"w")) == NULL ){
		printf("Error when opening the output file : errno %d (%s) .\n", errno, strerror(errno));
		return EXIT_FAILURE;
	}

	for(xpixel=0; xpixel<nbpixelx; xpixel++){ 
		for(ypixel=0; ypixel<nbpixely; ypixel++){ 

			double x = XMIN + xpixel * RESOLUTION; 
			double y = YMIN + ypixel * RESOLUTION; 
			fprintf(file, "%f %f %d\n", x, y, itertab[xpixel*nbpixely+ypixel]);

		}

		fprintf(file,"\n");

	} 

	fclose(file);

	printf("---- End of the program ----\n");

	// Program exit
	return EXIT_SUCCESS;
 }