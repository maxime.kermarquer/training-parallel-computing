
// Libraries
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <errno.h>
#include <sys/time.h> 

#include <omp.h>

// Define - Macros
#define OUTFILE "results/C-OpenMP-Mandelbrot.out"

double XMIN=-2;
double YMIN=-2;
double XMAX=2;
double YMAX=2;
double RESOLUTION=0.001;
int NITERMAX=5000;

int main(int argc,char **argv){

	int * itertab;
	int nbpixelx;
	int nbpixely;
	int xpixel=0,ypixel=0;
	FILE * file;

	if (argc > 2){
		RESOLUTION = atof(argv[1]);
		NITERMAX = atof(argv[2]);
	}


	// Computation of the number of pixels
	nbpixelx = ceil((XMAX - XMIN) / RESOLUTION);
	nbpixely = ceil((YMAX - YMIN) / RESOLUTION);

	printf("---- Start of the OpenMP program ----\nRESOLUTION = %f, NBITERMAX = %d, nbpixel = %d.\n\n", RESOLUTION, NITERMAX, nbpixelx*nbpixely);

	// Allocation of the pixel array
	if( (itertab=malloc(sizeof(int)*nbpixelx*nbpixely)) == NULL){
		printf("itertab allocation ERROR, errno : %d (%s) .\n", errno, strerror(errno)); 
		return EXIT_FAILURE;
	}


	// Points computation
	printf("----  Start of the points computation ----\n");
	struct timeval start, end, duration;
	gettimeofday(&start, NULL);

	double xinit;
	double yinit;
	double x;
	double y;
	int iter;
	double prevy,prevx;

	#pragma omp parallel private(xinit,yinit,x,y,prevy,prevx,iter,ypixel,xpixel) shared(nbpixely, nbpixelx,NITERMAX,itertab)
	for(xpixel=0;xpixel<nbpixelx;xpixel++){
			#pragma omp for 
			for(ypixel=0;ypixel<nbpixely;ypixel++) {
				 xinit = XMIN + xpixel * RESOLUTION;
				 yinit = YMIN + ypixel * RESOLUTION;
				 x=xinit;
				 y=yinit;
				for(iter=0;iter<NITERMAX;iter++){
					 prevy=y,prevx=x;
					if( (x*x + y*y) > 4 )
						break;
					x= prevx*prevx - prevy*prevy + xinit;
					y= 2*prevx*prevy + yinit;
				}
				itertab[xpixel*nbpixely+ypixel]=iter;
			}
	}
	

	gettimeofday(&end,NULL);
	timersub(&end, &start, &duration);
	printf("The computation lasted %ld.%ld seconds.\n---- End of the points computation ----\n\n",(long int)duration.tv_sec,(long int)duration.tv_usec);

	// Output of the results in a file compatible with gnuplot
	printf("---- Writing the file %s -----\n", OUTFILE);
	if( (file=fopen(OUTFILE,"w")) == NULL ){
		printf("Error when opening the output file : errno %d (%s) .\n", errno, strerror(errno));
		return EXIT_FAILURE;
	}

	for(xpixel=0; xpixel<nbpixelx; xpixel++){ 
		for(ypixel=0; ypixel<nbpixely; ypixel++){ 

			double x = XMIN + xpixel * RESOLUTION; 
			double y = YMIN + ypixel * RESOLUTION; 
			fprintf(file, "%f %f %d\n", x, y, itertab[xpixel*nbpixely+ypixel]);

		}

		fprintf(file,"\n");

	} 

	fclose(file);

	printf("---- End of the program ----\n");

	// Program exit
	return EXIT_SUCCESS;

}