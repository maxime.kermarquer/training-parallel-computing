
// Libraries
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <errno.h> 
#include <sys/time.h>

#include "mpi.h"

// Define - Macros
#define OUTFILE "results/C-MPI-Block-Mandelbrot.out"

double XMIN=-2;
double YMIN=-2;
double XMAX=2; 
double YMAX=2; 
double RESOLUTION=0.001;
int NITERMAX=5000;

int main(int argc, char **argv){

	int size, rank;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	int * itertab; 
	int * subitertab;
	int nbpixelx; 
	int nbpixely;
	int xpixel=0, ypixel=0; 
	FILE * file;

	if (argc > 2){
		RESOLUTION = atof(argv[1]);
		NITERMAX = atof(argv[2]);
	}

	// Computation of the number of pixels
	nbpixelx = ceil((XMAX - XMIN) / RESOLUTION);
	nbpixely = ceil((YMAX - YMIN) / RESOLUTION);

	if(rank == 0){
		printf("---- Start of the MPI Block program ----\nRESOLUTION = %f, NBITERMAX = %d, nbpixel = %d, NB_Process_MPI = %d.\n\n", RESOLUTION, NITERMAX, nbpixelx*nbpixely, size);
	}

	struct timeval start, end, duration;

	// Points computation
	if(rank == 0){
		printf("----  Start of the points computation ----\n");
		gettimeofday(&start, NULL);
	}

	// Allocation of the MPI process pixel array 
	if( (subitertab=malloc(sizeof(int)*nbpixelx*nbpixely/size)) == NULL){
		printf("subitertab allocation ERROR, errno : %d (%s) .\n", errno, strerror(errno)); 
		return EXIT_FAILURE;
	}

	for(xpixel = rank*nbpixelx/size; xpixel < (rank+1)*nbpixelx/size; xpixel++){
		for(ypixel=0; ypixel<nbpixely; ypixel++){
			double xinit = XMIN + xpixel * RESOLUTION;
			double yinit = YMIN + ypixel * RESOLUTION;

			double x=xinit; double y=yinit; int iter=0;

			for(iter=0; iter<NITERMAX; iter++){
				double prevy=y, prevx=x; 

				if( (x*x + y*y) > 4 )
					break;

				x= prevx*prevx - prevy*prevy + xinit;

				y= 2*prevx*prevy + yinit;
			}
			subitertab[(xpixel-(rank*nbpixelx/size))*nbpixely+ypixel] = iter;
		}
	}

	// Allocation of the pixel array to gather MPI process pixel array
	if( (rank == 0) && ((itertab=malloc(sizeof(int)*nbpixelx*nbpixely)) == NULL) ){
		printf("itertab allocation ERROR, errno : %d (%s) .\n", errno, strerror(errno)); 
		return EXIT_FAILURE; 
	}

	MPI_Gather(subitertab, (int)nbpixely*nbpixelx/size, MPI_INT, itertab, (int)nbpixely*nbpixelx/size, MPI_INT, 0, MPI_COMM_WORLD);

	// Free MPI process pixel array
	free(subitertab);

	if(rank == 0){

		gettimeofday(&end,NULL);
		timersub(&end, &start, &duration);
		printf("The computation lasted %ld.%ld seconds.\n---- End of the points computation ----\n\n",(long int)duration.tv_sec,(long int)duration.tv_usec);

		// Output of the results in a file compatible with gnuplot
		printf("---- Writing the file %s -----\n", OUTFILE);
		if( (file=fopen(OUTFILE,"w")) == NULL ){
			printf("Error when opening the output file : errno %d (%s) .\n", errno, strerror(errno));
			return EXIT_FAILURE;
		}

		for(xpixel=0; xpixel<nbpixelx; xpixel++){ 
			for(ypixel=0; ypixel<nbpixely; ypixel++){ 

				double x = XMIN + xpixel * RESOLUTION; 
				double y = YMIN + ypixel * RESOLUTION; 
				fprintf(file, "%f %f %d\n", x, y, itertab[xpixel*nbpixely+ypixel]);

			}

			fprintf(file,"\n");

		} 

		fclose(file);

		printf("---- End of the program ----\n");

	}

	MPI_Finalize();

	// Program exit
	return EXIT_SUCCESS;
 }